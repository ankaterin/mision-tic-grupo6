-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: dbMysql:3306
-- Generation Time: Sep 19, 2022 at 08:11 PM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `LaRockolaBD`
--

-- --------------------------------------------------------

--
-- Table structure for table `autor`
--

CREATE TABLE `autor` (
  `id_autor` int NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `autor`
--

INSERT INTO `autor` (`id_autor`, `nombre`) VALUES
(1, 'Marc Antony');

-- --------------------------------------------------------

--
-- Table structure for table `cancion`
--

CREATE TABLE `cancion` (
  `id_cancion` int NOT NULL,
  `fechadelanzamiento` varchar(255) DEFAULT NULL,
  `nombre_cancion` varchar(255) DEFAULT NULL,
  `id_autor` int DEFAULT NULL,
  `id_genero` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `cancion`
--

INSERT INTO `cancion` (`id_cancion`, `fechadelanzamiento`, `nombre_cancion`, `id_autor`, `id_genero`) VALUES
(1, '2011', 'Verdades', 1, 1),
(2, '2004', 'Que precio tiene el cielo', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `genero`
--

CREATE TABLE `genero` (
  `id_genero` int NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `genero`
--

INSERT INTO `genero` (`id_genero`, `nombre`) VALUES
(1, 'salsa');

-- --------------------------------------------------------

--
-- Table structure for table `playlist`
--

CREATE TABLE `playlist` (
  `id_playlist` int NOT NULL,
  `nombre_playlist` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `playlist`
--

INSERT INTO `playlist` (`id_playlist`, `nombre_playlist`) VALUES
(1, 'Salsa variada');

-- --------------------------------------------------------

--
-- Table structure for table `playlistcancion`
--

CREATE TABLE `playlistcancion` (
  `id_playlist_cancion` int NOT NULL,
  `id_cancion` int DEFAULT NULL,
  `id_playlist` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `playlistcancion`
--

INSERT INTO `playlistcancion` (`id_playlist_cancion`, `id_cancion`, `id_playlist`) VALUES
(1, 1, 1),
(3, 1, 1),
(4, 2, 1),
(5, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autor`
--
ALTER TABLE `autor`
  ADD PRIMARY KEY (`id_autor`);

--
-- Indexes for table `cancion`
--
ALTER TABLE `cancion`
  ADD PRIMARY KEY (`id_cancion`),
  ADD KEY `FKnle36k9ao7rdm9fcuhiayqegl` (`id_autor`),
  ADD KEY `FKtmli3ooy716hl8ak6ov9eu163` (`id_genero`);

--
-- Indexes for table `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`id_genero`);

--
-- Indexes for table `playlist`
--
ALTER TABLE `playlist`
  ADD PRIMARY KEY (`id_playlist`);

--
-- Indexes for table `playlistcancion`
--
ALTER TABLE `playlistcancion`
  ADD PRIMARY KEY (`id_playlist_cancion`),
  ADD KEY `FKcp4c8cbcnyys859b7rp6h0iba` (`id_cancion`),
  ADD KEY `FKh49wfse2trxnmtdhqyl287ip9` (`id_playlist`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `autor`
--
ALTER TABLE `autor`
  MODIFY `id_autor` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cancion`
--
ALTER TABLE `cancion`
  MODIFY `id_cancion` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `genero`
--
ALTER TABLE `genero`
  MODIFY `id_genero` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `playlist`
--
ALTER TABLE `playlist`
  MODIFY `id_playlist` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `playlistcancion`
--
ALTER TABLE `playlistcancion`
  MODIFY `id_playlist_cancion` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cancion`
--
ALTER TABLE `cancion`
  ADD CONSTRAINT `FKnle36k9ao7rdm9fcuhiayqegl` FOREIGN KEY (`id_autor`) REFERENCES `autor` (`id_autor`),
  ADD CONSTRAINT `FKtmli3ooy716hl8ak6ov9eu163` FOREIGN KEY (`id_genero`) REFERENCES `genero` (`id_genero`);

--
-- Constraints for table `playlistcancion`
--
ALTER TABLE `playlistcancion`
  ADD CONSTRAINT `FKcp4c8cbcnyys859b7rp6h0iba` FOREIGN KEY (`id_cancion`) REFERENCES `cancion` (`id_cancion`),
  ADD CONSTRAINT `FKh49wfse2trxnmtdhqyl287ip9` FOREIGN KEY (`id_playlist`) REFERENCES `playlist` (`id_playlist`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
