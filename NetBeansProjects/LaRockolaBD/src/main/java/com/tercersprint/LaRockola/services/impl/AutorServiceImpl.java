/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services.impl;

import com.tercersprint.LaRockola.models.Autor;
import com.tercersprint.LaRockola.dao.AutorDao;
import com.tercersprint.LaRockola.services.AutorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author DANNA
 */
@Service
public class AutorServiceImpl implements AutorService {
    @Autowired
    private AutorDao autorDao;

    @Override
    @Transactional (readOnly=true)
    public Autor save(Autor autor) {
      return autorDao.save( autor ); 
    }

    @Override
    @Transactional (readOnly=true)
    public void delete(Integer  id ) {
        autorDao.deleteById(id);    }

    @Override
    @Transactional (readOnly=true)
    public Autor findById(Integer id) {
        return autorDao.findById(id).orElse(null);    
    }

    @Override
    @Transactional (readOnly=true)
    public List<Autor> findAll() {
        return (List<Autor>) autorDao.findAll();
    }
}

    