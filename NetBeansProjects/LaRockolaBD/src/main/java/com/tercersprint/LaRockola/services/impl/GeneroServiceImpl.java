/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services.impl;

import com.tercersprint.LaRockola.models.Genero;
import com.tercersprint.LaRockola.dao.GeneroDao;
import com.tercersprint.LaRockola.services.GeneroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cata
 */
@Service
public class GeneroServiceImpl implements GeneroService{
    @Autowired
    private GeneroDao generoDao;

    @Override
    @Transactional (readOnly=true)
    public Genero save(Genero genero ) {
        return generoDao.save( genero );   
    }

    @Override
    @Transactional (readOnly=true)
    public void delete(Integer  id ) {
        generoDao.deleteById(id);    
    }

    @Override
    @Transactional (readOnly=true)
    public Genero findById(Integer id) {
        return generoDao.findById(id).orElse(null); 
    }

    @Override
    @Transactional (readOnly=true)
    public List<Genero> findAll() {
        return (List<Genero>) generoDao.findAll();    
    }
}
