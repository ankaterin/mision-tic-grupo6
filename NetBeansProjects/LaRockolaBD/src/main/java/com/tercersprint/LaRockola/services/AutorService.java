/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services;

import com.tercersprint.LaRockola.models.Autor;
import java.util.List;
/**
 *
 * @author cata
 */
public interface AutorService {
    
    public Autor save( Autor autor);
    public void delete( Integer id);
    public Autor findById(Integer id);
    public List<Autor> findAll();
}
