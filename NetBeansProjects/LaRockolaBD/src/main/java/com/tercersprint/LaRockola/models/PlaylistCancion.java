/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author DANNA
 */
@Table
@Entity(name = "Playlistcancion")
@Getter
@Setter
public class PlaylistCancion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idPlaylistCancion")
    private Integer PlaylistCancion;

    @ManyToOne
    @JoinColumn(name = "idCancion")
    private Cancion cancion;

    @ManyToOne
    @JoinColumn(name = "idPlaylist")
    private Playlist playlist;

}
