/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.controllers;

import com.tercersprint.LaRockola.services.AutorService;
import com.tercersprint.LaRockola.models.Autor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DANNA
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/autor")
public class AutorController {

    @Autowired
    private AutorService autorService;

    @GetMapping(value = "/list")
    public List<Autor> listarAutor() {
        return autorService.findAll();
    }

    @PostMapping(value = "/")
    public ResponseEntity<Autor> agregar(@RequestBody Autor autor) {
        Autor result = autorService.save(autor);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }
}
