/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.controllers;

import com.tercersprint.LaRockola.models.PlaylistCancion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tercersprint.LaRockola.services.PlaylistCancionService;

/**
 *
 * @author DANNA
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/playlist")
public class PlaylistCancionController {

    @Autowired
    private PlaylistCancionService playlistCancionService;

    @GetMapping(value = "/list")
    public List<PlaylistCancion> listarPlaylistCancion() {
        return playlistCancionService.findAll();
    }

    @PostMapping(value = "/list")
    public ResponseEntity<PlaylistCancion> agregar(@RequestBody PlaylistCancion playlistCancion) {
        PlaylistCancion result = playlistCancionService.save(playlistCancion);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
