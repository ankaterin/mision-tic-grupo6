/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services;

import com.tercersprint.LaRockola.models.Playlist;
import java.util.List;

/**
 *
 * @author cata
 */
public interface PlaylistService {
    
   public Playlist save( Playlist Playlist);
   public void delete( Integer id);
   public Playlist findById(Integer id);
   public List<Playlist> findAll();
    
}
