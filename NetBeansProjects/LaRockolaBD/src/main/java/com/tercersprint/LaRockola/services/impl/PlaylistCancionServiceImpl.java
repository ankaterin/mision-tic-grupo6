/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services.impl;

import com.tercersprint.LaRockola.models.PlaylistCancion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tercersprint.LaRockola.dao.PlaylistCancionDao;
import com.tercersprint.LaRockola.services.PlaylistCancionService;

/**
 *
 * @author DANNA
 */
@Service
public class PlaylistCancionServiceImpl implements PlaylistCancionService {

    @Autowired
    private PlaylistCancionDao playlistcancionDao;

    @Override
    @Transactional(readOnly = false)
    public PlaylistCancion save(PlaylistCancion playlistcancion) {
        return playlistcancionDao.save(playlistcancion);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        playlistcancionDao.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public PlaylistCancion findById(Integer id) {
        return playlistcancionDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PlaylistCancion> findAll() {
        return (List<PlaylistCancion>) playlistcancionDao.findAll();
    }
}
