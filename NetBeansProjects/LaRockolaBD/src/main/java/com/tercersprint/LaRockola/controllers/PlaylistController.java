/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.controllers;


import com.tercersprint.LaRockola.models.Playlist;
import com.tercersprint.LaRockola.services.PlaylistService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cata
 */
@RestController
@CrossOrigin ("*")
@RequestMapping("/Playlist")
public class PlaylistController {
     @Autowired
    private PlaylistService playlistService;
    
    @GetMapping ( value ="/list")
    public List<Playlist> listarGenero(){
        return playlistService.findAll();
    }
    @PostMapping( value ="/")
    
    public ResponseEntity <Playlist> agregar (@RequestBody Playlist playlist){
       Playlist result = playlistService.save(playlist);
       return new ResponseEntity<>(result,HttpStatus.OK);
    
}
}
