/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services;


import com.tercersprint.LaRockola.models.PlaylistCancion;
import java.util.List;

/**
 *
 * @author DANNA
 */
public interface PlaylistCancionService {
    
    public PlaylistCancion save( PlaylistCancion playlistcancion);
    public void delete( Integer id);
    public PlaylistCancion findById(Integer id);
    public List<PlaylistCancion> findAll();
}
