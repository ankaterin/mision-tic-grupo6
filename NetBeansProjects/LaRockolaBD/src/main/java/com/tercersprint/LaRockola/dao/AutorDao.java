/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.dao;

import com.tercersprint.LaRockola.models.Autor;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author cata
 */
public interface AutorDao extends CrudRepository<Autor, Integer> {
    
}
