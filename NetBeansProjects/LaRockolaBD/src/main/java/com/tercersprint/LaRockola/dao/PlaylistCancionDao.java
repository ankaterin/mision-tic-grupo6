
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.dao;

import com.tercersprint.LaRockola.models.PlaylistCancion;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author DANNA
 */
public interface PlaylistCancionDao extends CrudRepository<PlaylistCancion, Integer>{

    
}
