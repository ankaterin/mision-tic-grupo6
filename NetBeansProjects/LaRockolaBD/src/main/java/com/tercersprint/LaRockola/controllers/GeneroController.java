/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.controllers;

import com.tercersprint.LaRockola.models.Genero;
import com.tercersprint.LaRockola.services.GeneroService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DANNA
 */
@RestController
@CrossOrigin ("*")
@RequestMapping("/Genero")
public class GeneroController {
    @Autowired
    private GeneroService generoService;
    
    @GetMapping ( value ="/list")
    public List<Genero> listarGenero(){
        return generoService.findAll();
    }
    @PostMapping( value ="/")
    
    public ResponseEntity <Genero> agregar (@RequestBody Genero genero){
       Genero result = generoService.save(genero);
       return new ResponseEntity<>(result,HttpStatus.OK);
    
}
}
    
