/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.models;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;       
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author cata
 */
@Entity 
@Table (name="Autor")
@Getter
@Setter

public class Autor implements Serializable{
    
    @Id
    @GeneratedValue( strategy = GenerationType. IDENTITY )
    @Column(name="idAutor")
    private Integer idAutor;
    
    @Column(name="nombre")
    private String nombreAutor;
    
}

