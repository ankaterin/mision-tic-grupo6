/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services.impl;

import com.tercersprint.LaRockola.models.Playlist;
import com.tercersprint.LaRockola.dao.PlaylistDao;
import com.tercersprint.LaRockola.services.PlaylistService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author DANNA
 */
@Service
public class PlaylistServiceImpl implements PlaylistService {
    @Autowired
    private PlaylistDao playlistDao;

    @Override
    @Transactional (readOnly=true)
    public Playlist save(Playlist playlist ) {
        return playlistDao.save( playlist );
    }

    @Override
    @Transactional (readOnly=true)
    public void delete(Integer id ) {
        playlistDao.deleteById(id); 
    }

    @Override
    @Transactional (readOnly=true)
    public Playlist findById(Integer id) {
        return playlistDao.findById(id).orElse(null);  
   }

    @Override
    @Transactional (readOnly=true)
    public List<Playlist> findAll() {
        return (List<Playlist>) playlistDao.findAll();    
    }
}
