/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.services.impl;

import com.tercersprint.LaRockola.models.Cancion;
import com.tercersprint.LaRockola.dao.CancionDao;
import com.tercersprint.LaRockola.services.CancionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cata
 */
@Service
public class CancionServiceImpl implements CancionService {
    @Autowired
    private CancionDao cancionDao;

    @Override
    public Cancion save(Cancion cancion ) {
        return cancionDao.save( cancion);
    }

    @Override
    public void delete(Integer id ) {
        cancionDao.deleteById(id);    
    }

    @Override
    @Transactional (readOnly=true)
    public Cancion findById(Integer id) {
        return cancionDao.findById(id).orElse(null);    
    }

    @Override
    @Transactional (readOnly=true)
    public List<Cancion> findAll() {
        return (List<Cancion>) cancionDao.findAll();   
    }
}
