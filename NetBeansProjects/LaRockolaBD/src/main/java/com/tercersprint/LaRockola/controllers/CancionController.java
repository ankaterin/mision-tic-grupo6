/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tercersprint.LaRockola.controllers;

import com.tercersprint.LaRockola.services.CancionService;
import com.tercersprint.LaRockola.models.Cancion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DANNA
 */
@RestController
@CrossOrigin("*")
@RequestMapping("api/v1/canciones")
public class CancionController {

    @Autowired
    private CancionService cancionService;

    @GetMapping(value = "/")
    public List<Cancion> listarCanciones() {
        return cancionService.findAll();
    }

    @PostMapping(value = "/")
    public ResponseEntity<Cancion> agregar(@RequestBody Cancion album) {
        Cancion result = cancionService.save(album);
        return new ResponseEntity<>(result, HttpStatus.OK);

    }

    @DeleteMapping(value = "/{idCancion}")
    public ResponseEntity<Void> eliminar(@PathVariable("idCancion") Integer idCancion) {
        cancionService.delete(idCancion);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}